<?php
$_['text_theme_menu'] = "Template";

$_['text_theme_config'] = "Configuração do Tema";

$_['text_category_thumbnail'] = "Categorias Destaque";

$_['text_advance_menu'] = 'Menu da Loja';

$_['text_blog'] = "Blog";
$_['text_article'] = "Artigos";
$_['text_article_list'] = "Lista de Artigos";
$_['text_blog_config'] = "Configuração";

$_['text_cms_block'] = "Blocos de CMS";

$_['text_testimonial'] = "Depoimentos";

$_['text_slideshow'] = "Sliders";