<?php
// Heading
$_['heading_title'] = 'Orçamento cadastrado';

// Text
$_['text_basket']   = 'Carrinho de orçamentos';
$_['text_checkout'] = 'Finalizar orçamento';
$_['text_success']  = 'Confirmação';
$_['text_customer'] = '<p>O seu orçamento foi cadastrado em nossa loja.</p><p>Você pode ver o histórico dos seus orçamentos através de sua <a href="%s">conta</a>, acessando o menu <a href="%s">histórico de pedidos</a>.</p><p>Caso seu orçamento esteja associado a arquivos para download, acesse em sua conta o menu <a href="%s">downloads</a> para baixá-los.</p><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado por comprar em nossa loja.</p>';
$_['text_guest']    = '<p>O seu orçamento foi cadastrado em nossa loja.</p><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado por comprar em nossa loja.</p>';