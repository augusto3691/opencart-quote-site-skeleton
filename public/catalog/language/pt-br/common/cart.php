<?php
// Text
$_['text_items']     = '%s - %s';
$_['text_empty']     = 'Seu carrinho está vazio.';
$_['text_cart']      = 'Exibir carrinho';
$_['text_checkout']  = 'Finalizar orçamento';
$_['text_recurring'] = 'Assinatura';