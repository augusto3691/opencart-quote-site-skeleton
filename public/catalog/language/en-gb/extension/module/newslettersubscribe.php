<?php
// Heading 
$_['heading_title']  = 'Newsletter';
$_['heading_title2'] = 'Receba notícias e novidades de nossos produtos';
$_['heading_title3'] = 'Assine Já';

$_['newletter_des']  = 'Insira seu email e receba notícias e novidades de nossos produtos';
$_['newletter_des2'] = 'Assine nossa Newsletter e receba notícias e novidades de nossos produtos';

//Fields
$_['entry_email'] = 'Seu email aqui...';
$_['entry_name']  = 'Name';

//Buttons
$_['entry_button']   = 'Assinar';
$_['entry_unbutton'] = 'Descadastrar';

//text
$_['text_subscribe'] = 'Assine Aqui';

$_['mail_subject'] = 'Assinatura Newsletter';

//Messages
$_['error_invalid']    = 'Error : Por Favor entre um emal válido.';
$_['subscribe']        = 'Success : Obrigado, aguarde para receber novidades.';
$_['unsubscribe']      = 'Success : Descadastrado da Newsletter.';
$_['alreadyexist']     = 'Error : Email já cadastrado.';
$_['notexist']         = 'Error : Email inexistente.';
$_['entry_show_again'] = "Não mostrar mais essa mensagem";