<?php
// Heading
$_['heading_title'] = 'Trending products';
$_['module_description'] = 'Trending products';

// Text
$_['text_reviews']          = '%s reviews';
$_['text_sale']      = 'Sale';
$_['text_new']      = 'New';
$_['text_empty_categories'] = 'There is no featured categories';
$_['text_empty_products'] 	= 'There is no products in this category';